                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1748685
Compressed Air Rocket Nose Design by ttd is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Students will design various noses for the Compressed Air Rocket system. Optionally following the NASA Glen Research Center's Science of Flight program, students consider physics and mathematics while designing their nose cones.

# How I Designed This

The examples nose cones are based on the NASA Glen Research Center Science of Flight documents. The .stl included are the cone shape, blunt shape and parabola. 

Thin, lightweight PLA is brittle when crashing into hard concrete. Be sure to launch these rockets over grassy areas.

# Project: Compressed Air Rocket Nose Design

Students may experiment with the included .stl of the parabola, cone or blunt nose shapes. Following the NASA documentation, there are still many other shapes for students to design and experiment with as well.

![Alt text](https://cdn.thingiverse.com/assets/6c/91/f7/8d/2f/3NoseDesigns.jpg)

## Overview & Background

Educators will have a 3D printed nose design that is more sturdy than a paper nose. Typically paper noses are crushed after just one flight. PLA printed noses will allow students more chances to experiment. The lightweight nature of PLA also allows the rockets to still fly high.



![Alt text](https://cdn.thingiverse.com/assets/ff/7a/01/33/e3/FPLY3WKIACFKPPV.MEDIUM.jpg)

## Objectives

Depending the grade level, students may be Kindergartners having fun seeing their designs fly into the sky all the way up to upper grade level students using physics equations to calculate the effects of their designs upon flight.


![Alt text](https://cdn.thingiverse.com/assets/82/26/f5/0f/d0/nose-shapes.gif)

<iframe src="//www.youtube.com/embed/gmvNHMpiCWo" frameborder="0" allowfullscreen></iframe>

## Audiences

K-12 with differentiation for each grade level.

## Subjects

Science, Math

## Skills Learned (Standards)

K-2-ETS1-1.	Ask questions, make observations, and gather information about a situation people want to change to define a simple problem that can be solved through the development of a new or improved object or tool.
K-2-ETS1-2.	Develop a simple sketch, drawing, or physical model to illustrate how the shape of an object helps it function as needed to solve a given problem.
K-2-ETS1-3.	Analyze data from tests of two objects designed to solve the same problem to compare the strengths and weaknesses of how each performs.

Students who demonstrate understanding can:
3-5-ETS1-1.	Define a simple design problem reflecting a need or a want that includes specified criteria for success and constraints on materials, time, or cost.
3-5-ETS1-2.	Generate and compare multiple possible solutions to a problem based on how well each is likely to meet the criteria and constraints of the problem.
3-5-ETS1-3.	Plan and carry out fair tests in which variables are controlled and failure points are considered to identify aspects of a model or prototype that can be improved.

MS-ETS1-1.	Define the criteria and constraints of a design problem with sufficient precision to ensure a successful solution, taking into account relevant scientific principles and potential impacts on people and the natural environment that may limit possible solutions.
MS-ETS1-2.	Evaluate competing design solutions using a systematic process to determine how well they meet the criteria and constraints of the problem.
MS-ETS1-3.	Analyze data from tests to determine similarities and differences among several design solutions to identify the best characteristics of each that can be combined into a new solution to better meet the criteria for success.
MS-ETS1-4.	Develop a model to generate data for iterative testing and modification of a proposed object, tool, or process such that an optimal design can be achieved.

Students who demonstrate understanding can:
HS-ETS1-1.	Analyze a major global challenge to specify qualitative and quantitative criteria and constraints for solutions that account for societal needs and wants.
HS-ETS1-2.	Design a solution to a complex real-world problem by breaking it down into smaller, more manageable problems that can be solved through engineering.
HS-ETS1-3.	Evaluate a solution to a complex real-world problem based on prioritized criteria and trade-offs that account for a range of constraints, including cost, safety, reliability, and aesthetics as well as possible social, cultural, and environmental impacts.
HS-ETS1-4.	Use a computer simulation to model the impact of proposed solutions to a complex real-world problem with numerous criteria and constraints on interactions within and between systems relevant to the problem.

<iframe src="//www.youtube.com/embed/eNFfK5uo6D0" frameborder="0" allowfullscreen></iframe>

## Lesson/Activity

Duration:
Compressed Air Rocket Nose Design could be as simple as the 5 minutes with the educator pre-printing everything and the students simply launching them to compare. Or it could be more in depth following the NASA Glen Research Center program on Science of Flight to calculate the effects of design on rocket flight.

Preparation:
Educators may pre-print and construct the rockets as well as the launcher or let students complete all of that depending on what is appropriate for the grade level.

The Compressed Air Rocket Launcher setup is based on the one from Air Rocket Works. It utilizes the same launcher many people see on MakerShed and at Maker Faire across the world. It is similar to the design that can be built from Make Magazine with DIY pieces from a hardware store. A kit of the components from Air Rocket Works can also be ordered from MakerShed and assembled in about 30-60 minutes.

## References

Air Rocket Works:
http://www.airrocketworks.com/

MakerShed:
http://www.makershed.com/products/compressed-air-rocket-launcher-v2-1

NASA Advanced High-Power Paper Rockets:
https://www.nasa.gov/pdf/295786main_Rockets_Adv_High_Power_Paper.pdf

NASA Rocket Modeler III:
https://www.grc.nasa.gov/WWW/K-12/rocket/rktsim.html



## Rubric & Assessment

Depending grade levels, students may be cheering for successful rocket launches. They may have data on the flight time or height of rocket launches. They may also have hypothesis from calculations that they have test and proved or disproved.